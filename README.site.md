
# Static site starter contents for `reploy`

Use this repository as a base for creating new sites deployed by `reploy`.

To use this, simply modify whatever you do not like in the pages directory.

Do not forget about:
- writing a good title page in `pages/index.md`
- filling in the content license for your site in `pages/meta/license.md`

Ideally, you also want to keep some connection to this original site (fork
relation, remote, etc.), which helps with ingesting the updates created in this
repo. In the future, all files will be kept preferably in the very same place
as now, to avoid conflicts with other repositories, allowing also multi-origin
merged repositories.

