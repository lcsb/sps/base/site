---
mount: /playground
name: Playground
---

# Playground

You want to erase this page before your site goes live :)

Headers:

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

Lists:

- list
- list
  - sublist
  - sublist
- list again

Numbered lists:

1. this
2. that
3. these

[This link](/) links to the main page (matches the `mount` there).

You can actually write `inline code` and also block code:

```sh
#!/bin/bash

echo "oh hello this is a successful script"
exit 0
```

Tables are possible using the "pipe tables":

| Item         | Price | # In stock |
|--------------|:-----:|-----------:|
| Juicy Apples |  1.99 |        739 |
| Bananas      |  1.89 |          6 |

(table taken from [this website](https://markdown.land/markdown-table))

You may _also_ *highlight* things and **very** __gravely__ highlight things, but do not overuse that.
