---
mount: /
name: Home
toc: off
timestamp: null
---

# Well hello there!

This is the front page of your new site. If you see this in your browser,
everything went well and you can start adding new contents!

See [the playground](/playground) for examples of formatting.
